import fs from "fs";
import readline from "readline";
import colors from "colors";

interface IObject {
	[key: string]: string | number | boolean | any | null | {};
}

const parseValue = (value: string) => {
	if (!value) return "";

	if (!isNaN(Number(value))) {
		if (value.includes(".") && !value.match(/^[0-9]+\.[0-9]$/)) {
			return value;
		}
		return Number(value);
	}

	switch (value) {
		case "true":
			return true;
		case "false":
			return false;
		case "null":
			return null;
		case "undefined":
			return undefined;
		default:
			return value;
	}
};

const input = readline.createInterface({
	input: process.stdin,
	output: process.stdout,
});

const reducer = (
	prev: IObject,
	curr: string,
	idx: number,
	arr: string[],
	value: string,
	rowIdx: number
): any => {
	const fullKey = `"${arr.join(".")}"`;
	const fileRow = rowIdx + 1;

	if (!curr.match(/^[0-9a-zA-Z]+$/)) {
		console.log(
			colors.bgYellow.black(
				`row(${fileRow}): Not alphanumeric key ${curr}. Row ignored: `
			),
			colors.bgBlack.yellow(fullKey)
		);
		return { ...(prev[curr] ?? {}) };
	}

	if (idx === arr.length - 1) {
		const hasInvalidChars = /\s/g.test(value) || /=/g.test(value);
		const isValueInQuotes = value[0] === '"' && value[value.length - 1] === '"';

		if (hasInvalidChars && !isValueInQuotes) {
			console.log(
				colors.bgYellow.black(
					`row(${fileRow}): Invalid char "'='|' '" in value ${curr}. Row ignored: `
				),
				colors.bgBlack.yellow(fullKey)
			);
			return { ...(prev[curr] ?? {}) };
		}

		value = value.trim();
		value = `${value[0].replace(/\"/, "")}${value.slice(1, value.length - 1)}${value[
			value.length - 1
		].replace(/\"/, "")}`;

		const nextValue = parseValue(value);
		if (nextValue === undefined) {
			console.log(
				colors.bgYellow.black(`row(${fileRow}): Undefined value with key: `),
				colors.bgBlack.yellow(fullKey)
			);
		}
		return (prev[curr] = nextValue);
	}

	return (prev[curr] = { ...(prev[curr] ?? {}) });
};

const readFileData = (
	file: string
): { result: IObject; filepath: string; filename: string } => {
	const rootObject: IObject = {};
	const filepath = file.split("/").slice(0, -1).join("/") || ".";
	const filename = file.split("/").slice(-1)[0].slice(0, -4);
	try {
		const data = fs.readFileSync(file, { encoding: "utf-8" });
		if (!data) {
			throw new Error("It looks like the file is empty :(");
		}
		data.split(/\r\n/).forEach((row: string, rowIdx: number) => {
			const splittedRow = row.split("=");
			if (splittedRow.length >= 2) {
				const [key, value] =
					splittedRow.length === 2
						? splittedRow
						: [splittedRow[0], [...splittedRow.slice(1)].join("=")];
				key
					.split(".")
					.reduce(
						(previousValue, currentValue, currentIndex, array) =>
							reducer(previousValue, currentValue, currentIndex, array, value, rowIdx),
						rootObject
					);
			}
		});
	} catch (err) {
		throw new Error(`Error while reading file: ${(err as Error).message}`);
	}
	return {
		result: rootObject,
		filepath,
		filename,
	};
};

const writeToJSON = (data: IObject, filepath: string, filename: string) => {
	if (!fs.existsSync("./output")) {
		fs.mkdirSync("./output");
	}

	try {
		const fd = fs.openSync(`${filepath}/output/${filename}.json`, "w");
		fs.writeFileSync(fd, JSON.stringify(data));
		console.log(
			colors.bgGreen.black(`The result is in ${filepath}/output/${filename}.json`)
		);
		fs.closeSync(fd);
		start();
	} catch (err) {
		throw new Error(
			`Failed to open / create ${filepath}/output/${filename}.json: ${
				(err as Error).message
			}`
		);
	}
};

const start = () => {
	input.question("Enter file name (.txt only): ", (file) => {
		if (file.slice(-4) !== ".txt") {
			console.log(
				colors.bgRed.black("Sorry, i'm working only with .txt :( Please, try again.")
			);
			return start();
		}

		try {
			const { result, filepath, filename } = readFileData(file);
			writeToJSON(result, filepath, filename);
		} catch (err) {
			console.log(colors.bgRed.black((err as Error).message));
		} finally {
			start();
		}
	});
};

start();
